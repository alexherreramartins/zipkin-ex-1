package br.com.mastertech.user.dto;

public class CreateUserRequest {

    private String cep;
    private String nome;

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
