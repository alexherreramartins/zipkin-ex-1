package br.com.mastertech.user.dto;

import br.com.mastertech.user.client.Cep;
import br.com.mastertech.user.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {


    public User toUser(CreateUserRequest createUserRequest, Cep cep) {
        User user = new User();

        user.setCep(createUserRequest.getCep());
        user.setNome(createUserRequest.getNome());
        user.setBairro(cep.getBairro());
        user.setComplemento(cep.getComplemento());
        user.setLocalidade(cep.getLocalidade());
        user.setLogradouro(cep.getLogradouro());
        user.setUf(cep.getUf());

        return user;

    }
}
