package br.com.mastertech.user.service;

import br.com.mastertech.user.model.User;
import br.com.mastertech.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;


    public User create(User user){
        return userRepository.save(user);
    }
}
