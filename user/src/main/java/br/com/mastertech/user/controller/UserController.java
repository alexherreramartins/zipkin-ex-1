package br.com.mastertech.user.controller;

import br.com.mastertech.user.client.Cep;
import br.com.mastertech.user.client.ConsultaCepClient;
import br.com.mastertech.user.dto.CreateUserRequest;
import br.com.mastertech.user.dto.UserMapper;
import br.com.mastertech.user.model.User;
import br.com.mastertech.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private ConsultaCepClient consultaCepClient;

    @Autowired
    private UserService userService;

    @Autowired
    private UserMapper userMapper;


    @PostMapping
    public User createUser(@RequestBody CreateUserRequest createUserRequest){

        Cep cep = consultaCepClient.getCep(createUserRequest.getCep());

        User user = userMapper.toUser(createUserRequest,cep);

        return userService.create(user);

    }


}
