package br.com.mastertech.user.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cep")
public interface ConsultaCepClient {

    @GetMapping("/cep/{cep}")
    Cep getCep(@PathVariable String cep);
}
