package br.com.mastertech.consultacep.controller;


import br.com.mastertech.consultacep.client.Cep;
import br.com.mastertech.consultacep.client.CepClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;

@RestController
@RequestMapping("/cep")
public class ConsultaController {

    @Autowired
    private CepClient cepClient;

    @GetMapping("/{cep}")
    public Cep getAddress(@PathVariable @NotBlank String cep){
        return cepClient.getCep(cep);
    }

}
